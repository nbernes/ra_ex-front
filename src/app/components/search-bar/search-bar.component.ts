import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'searchBar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  @Output() newURLEvent = new EventEmitter<string>();
  searchBar = new FormControl("", [Validators.pattern(/^https:\/\/www.youtube.com\/watch\?v=\S{11}$/), Validators.required]);
  
  constructor() { }

  ngOnInit() {
  }

  submitURL() {
    this.newURLEvent.emit(this.searchBar.value.split('=').pop());
    this.searchBar.setValue("");
  }
}
