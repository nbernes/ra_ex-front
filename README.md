# Relief Applications exercise (front-end)
Angular front-end for the YouTube video viewer Relief Applications exercise.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.2.

## Installation

### Local / development setup
Requires you to have **npm** and [Angular](https://angular.io/guide/setup-local) installed.

To install dependencies, run at the root of the repository:
```sh
npm install
```
Then, to launch the Angular server, run:
```sh 
npm start
```
You can then access the webpage on http://localhost:4200.

### Docker / production setup

Requires that you have **Docker** installed.

At the root of the repository, run:
```sh
docker build -t videoviewer .
docker run -p 4200:4200 videoviewer
```
You can then access the webpage on http://localhost:4200.