import { Component } from '@angular/core';
import { VideoService } from './services/video.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  currentVideo: string = "";
  showBookmarks: boolean = false;
  currentVideoBookmarked: boolean = false;
  nbOfBookmarks: number = 0;

  constructor(
    private vs: VideoService
  ) {
    this.vs.fetchBookmarks();
    this.vs.nbOfBookmarks$.subscribe({
      next: (res: number) => this.nbOfBookmarks = res,
      error: (err: Error) => console.error(err)
    });
  }

  onNewVideo(video: string) {
    this.vs.addHistoryItem(video).subscribe({
      next: (_) => {
        this.vs.fetchHistory();
        this.currentVideo = video;
        if (this.vs.isVideoBookmarked(this.currentVideo))
          this.currentVideoBookmarked = true;
        else
          this.currentVideoBookmarked = false;
      },
      error: (err: Error) => console.error(err)
    });
  }

  bookmarkVideo() {
    this.vs.addBookmark(this.currentVideo).subscribe({
      next: (_) => {
        this.currentVideoBookmarked = true;
        this.vs.fetchBookmarks();
      },
      error: (err: Error) => console.error(err)
    })
  }

}
