import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

import { ItemResponse } from '../interfaces/item-response';


@Injectable({
  providedIn: 'root'
})
export class VideoService {
  private _baseUrl = environment.backendUrl;

  private readonly _bookmarksSource = new BehaviorSubject<Array<string>>([]);
  readonly bookmarks$ = this._bookmarksSource.asObservable();

  private readonly _historySource = new BehaviorSubject<Array<string>>([]);
  readonly history$ = this._historySource.asObservable();

  private readonly _nbOfBookmarks = new BehaviorSubject<number>(0);
  readonly nbOfBookmarks$ = this._nbOfBookmarks.asObservable();

  constructor(
    private http: HttpClient
  ) { }

  getBookmarks(): Array<string> {
    return this._bookmarksSource.getValue();
  }
  
  isVideoBookmarked(video: string): boolean {
    return this.getBookmarks().includes(video);
  }

  fetchBookmarks(): void {
    const reqUrl = `${this._baseUrl}/bookmarks/`;
    this.http.get<Array<ItemResponse>>(reqUrl)
      .subscribe({
        next: (res: Array<ItemResponse>) => {
          this._bookmarksSource.next(res.map(x => x.video_id));
          this._nbOfBookmarks.next(res.length);
        }, 
        error: (err: Error) => console.error(err)
      });
  }

  addBookmark(bookmark: string): Observable<any> {
    const reqUrl = `${this._baseUrl}/bookmarks/`;
    return this.http.post(reqUrl, {
      video_id: bookmark
    }); 
  }

  getHistory(): Array<string> {
    return this._historySource.getValue();
  }

  fetchHistory(): void {
    const reqUrl = `${this._baseUrl}/history/`;
    this.http.get<Array<ItemResponse>>(reqUrl)
      .subscribe({
        next: (res: Array<ItemResponse>) => {
          this._historySource.next(res.map(x => x.video_id).reverse());
        }, 
        error: (err: Error) => console.error(err)
      });
  }

  addHistoryItem(item: string): Observable<any> {
    const reqUrl = `${this._baseUrl}/history/`;
    return this.http.post(reqUrl, {
      video_id: item
    }); 
  }
}
