import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { VideoService } from 'src/app/services/video.service';

@Component({
  selector: 'Bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.scss']
})
export class BookmarksComponent implements OnInit {
  
  @Output() bookmarkedClicked = new EventEmitter<string>();
  bookmarks = Array<string>();


  constructor(
    private vs: VideoService
  ) { 
    this.vs.bookmarks$.subscribe({
      next: (items: Array<string>) => {
        this.bookmarks = items;
      },
      error: (err: Error) => console.error(err) 
    });
  }

  ngOnInit() {
    this.vs.fetchBookmarks();
  }

}
