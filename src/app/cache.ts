import Dexie, { Table } from 'dexie';

import { ItemResponse } from './interfaces/item-response';

export interface History {
  id?: number;
  items: Array<ItemResponse>;
}

export interface Bookmark {
  id?: number;
  created: Date;
  video_id: string;
}

export class Cache extends Dexie {
  history: Table<History, number>;

  constructor() {
    super('Cache');
    this.version(1).stores({
      history: 'id, items',
      bookmarks: '++id, created, video_id'
    });
    this.history = this.table('history');
    this.on('populate', () => this.populate());
  }

  private async populate() {
    const historyId = await cache.history.add({
      id: 1,
      items: []
    });
  }
}

export const cache = new Cache();