import { Component, OnInit, OnChanges, Input, HostListener } from '@angular/core';

@Component({
  selector: 'videoView',
  templateUrl: './video-view.component.html',
  styleUrls: ['./video-view.component.scss']
})
export class VideoViewComponent implements OnInit {

  @Input() videoId: string | undefined = undefined;
  isOffline = false;

  constructor() { }

  ngOnInit() {
    const tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api';
    document.body.appendChild(tag);
  }

  @HostListener('window:offline')
  onWindowOffline() {
    this.isOffline = true;
  }

  @HostListener('window:online')
  onWindowOnline() {
    this.isOffline = false;
  }
}
