'use strict';

var IDB = class {
  dbInstance;

  constructor(
    dbName, 
    version,
    onUpgradeCallbacks = []
  ) {
    this.dbName = dbName;
    this.version = version;
    this.onUpgradeCallbacks = onUpgradeCallbacks;
    this._getDB();
  }

  _getDB() {
    if (this.dbInstance) return this.dbInstance;
    
    this.dbInstance = new Promise((resolve, reject) => {
      const openreq = self.indexedDB.open(this.dbName, this.version);
      
      openreq.onerror = () => {
        reject(openreq.error);
      };
      
      openreq.onupgradeneeded = () => {
        for (let callback of this.onUpgradeCallbacks) { 
          callback(openreq.result);
        }
      };
      
      openreq.onsuccess = () => {
        resolve(openreq.result);
      };
    });
    return this.dbInstance;
  }

  async _openForTransaction(type, key, callback) {
    const db = await this._getDB();

    return new Promise((resolve, reject) => {
      const transaction = db.transaction(key, type);
      transaction.oncomplete = () => resolve();
      transaction.onerror = () => reject(transaction.error);
      callback(transaction.objectStore(key));
    });
  }

  async iterateThroughStoreItems(transactionType, storeKey, onSuccess) {
    const db = await this._getDB();
    const transaction = db.transaction(storeKey, transactionType);
    const objectStore = transaction.objectStore(storeKey);
    objectStore.openCursor().onsuccess = onSuccess;
  }; 


  async list(key) {
    let request;
    await this._openForTransaction('readonly', key, (store) => {
      request = store.getAll();
    });
    return request.result;
  }


  create(storeKey, val) {
    return this._openForTransaction('readwrite', storeKey, (store) => {
      store.add(val);
    });
  }

  delete(storeKey, key) {
    return this._openForTransaction('readwrite', storeKey, (store) => {
       store.delete(key);
    });
  }
}


var Store = class {
  idb;

  constructor() {
    const storeVersion = 1;
    const onUpgradeCallbacks = [
      (db) => db.createObjectStore("historyItems", {autoIncrement: true}),
      (db) => db.createObjectStore("bookmarks", {autoIncrement: true}),
      (db) => db.createObjectStore("apiCalls", {autoIncrement: true}),
    ]
    this.idb = new IDB('video-viewer', storeVersion, onUpgradeCallbacks);
  }

  getIdb = () => this.idb;

  fetchHistory = async () => {
    const historyItems = await this.idb.list('historyItems');
    return historyItems.map(item => { return {video_id: item}; });
  }

  fetchBookmarks = async () => {
    const bookmarks = await this.idb.list('bookmarks');
    return bookmarks.map(item => { return {video_id: item}; });
  }

  addHistoryItem = async (val) => {
    return await this.idb.create('historyItems', val);
  }

  addBookmark = async (val) => {
    return await this.idb.create('bookmarks', val);
  }

  registerAPICall = async (val) => {
    await this.idb.create('apiCalls', val);
  }
}


var DataAccessor = class {
  constructor() {
    this.store = new Store();
  }

  formatAPIReq = async (req) => {
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json;charset=utf-8',
    };
    let apiReq = {
      init: {
        method: req.method,
        headers: headers,
      },
      url: req.url
    }
    if (req.method !== 'GET') {
      const body = JSON.stringify(await req.json());
      apiReq.init.body = body;
    }
    return JSON.stringify(apiReq);
  }

  newResponse = (status, statusText = "", body = {}) => {
    const init = {
      status: status,
      statusText: statusText,
      ok: (status < 400) ? true : false,
      headers: {'Content-Type': 'application/json'},
    }
    return new Response(JSON.stringify(body), init);
  }

  safeFetch = async (url, init) => {
    try {
      return fetch(url, init);
    } catch (err) {
      return this.newResponse(504, "Gateway Timeout");
    }
  }

  async sendPOSTReqs(reqs) {
    const db = await this.store.getIdb();
    let errorOccured = false;

    for (let [key, val] of Object.entries(reqs)) {
      try {
        const res = await this.safeFetch(val.url, val.init);

        if (res.ok)
          await db.delete('apiCalls', parseInt(key));
        else
          errorOccured = true;
      } catch (_) {
        errorOccured = true;
      } 
    }
  }

  async replayPOSTReqs(newReq = null) {
    if (newReq)
      await this.store.registerAPICall(await this.formatAPIReq(newReq));
    
    let reqs = {};
    
    await this.store.getIdb().iterateThroughStoreItems('readwrite', 'apiCalls', 
      // onsuccess
      (event) => {
        const cursor = event.target.result;
        if (!cursor) {
          this.sendPOSTReqs(reqs);
          return;
        }
        reqs[cursor.key] = JSON.parse(cursor.value);
        cursor.continue();
      }
    );
  }

  handlePOSTRequest = async(req) => {
    this.replayPOSTReqs(req.clone());
    
    const body = await req.json();

    if (req.url.includes('history')) {
      const res = await this.store.addHistoryItem(body.video_id);
      return this.newResponse(200, "OK", res);
    } else if (req.url.includes('bookmarks')) {
      const res = await this.store.addBookmark(body.video_id);
      return this.newResponse(200, "OK", res);
    }
    const formattedReq = await this.formatAPIReq(req);
    return await this.safeFetch(formattedReq.url, formattedReq.init);
  }

  fetchFromStore = async (req) => {
    if (req.url.includes('history')) {
      const res = await this.store.fetchHistory();
      return this.newResponse(200, "OK", res);
    }
  
    else if (req.url.includes('bookmarks')) {
      const res = await this.store.fetchBookmarks();
      return this.newResponse(200, "OK", res);
    }
   
    return this.newResponse(400, "Invalid request");
  }

  handleGETRequest = async (req) => {
    try {
      return fetch(req)
        .then(async res => {
          if (res.ok)
            return this.newResponse(res.status, res.statusText, await res.json());
          return this.fetchFromStore(req);
        }).catch(_ => {
          return this.fetchFromStore(req);
        });
    } catch(_) {
      return this.fetchFromStore(req);
    }
  }


  handleAPIRequest = async (req) => {
    switch (req.method) {
      case 'GET':
        return await this.handleGETRequest(req);
      case 'POST':
        return await this.handlePOSTRequest(req);
      default:
        return await fetch(req);
    }
  }
}



const apiUrl = 'http://localhost:8000';
var acs;

  
self.addEventListener('activate', function(event) {
  acs = new DataAccessor();
});


self.addEventListener('fetch', function(event) {
  if (event.request.url.includes(apiUrl)) {
    event.respondWith(acs.handleAPIRequest(event.request));
  }
});