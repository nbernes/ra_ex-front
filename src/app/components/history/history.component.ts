import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { VideoService } from 'src/app/services/video.service';

@Component({
  selector: 'History',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  @Output() historyItemClicked = new EventEmitter<string>();
  historyItems = Array<string>();

  constructor(
    private vs: VideoService
  ) {
    this.vs.history$.subscribe({
      next: (items: Array<string>) => this.historyItems = items,
      error: (err: Error) => console.error(err) 
    });
  }

  ngOnInit() {
    this.vs.fetchHistory();
  }

}
